---
title: "Trisect MVP"
summary: "Hitting the Trisect MVP"
date: 2022-08-01
resource:
 - src: "*.png"
---

In preparation for upcoming testing in Corvallis I've been pulling together a minimum viable product of the [trisect_sw stack]({{< ref "Software_User" >}}).

Right now the stack can:

* Capture hardware synchronized monochrome stereo (~HD resolution) at 10Hz with ROS control over camera gain and exposure settings.  10Hz is driven primarily by the throughput of the stereo processing algorithm.
* Calculate stereo depth using SGBM from NVidia Visionworks, output as a disparity image or point cloud.   This currently runs at frame rate when decimated ~4x in each direction.   It includes optionally:
  * GPU-accelerated [bilateral disparity filter](https://docs.opencv.org/4.6.0/d8/d4f/classcv_1_1cuda_1_1DisparityBilateralFilter.html), or
  * GPU-accelerated [weighted-least-squares filter](https://docs.opencv.org/4.6.0/d9/d51/classcv_1_1ximgproc_1_1DisparityWLSFilter.html)
* Capture color imagery (max frame rate/resolution not explored)
* Expose all data (imagery, disparity, point clouds) via ROS
* Expose image streams via [web_video_server]({{< relref "Non-ROS_Outputs" >}})

It cannot:

* Project color onto the point cloud
* Provide the [I2C Sensor data]({{< relref "I2C_Sensors" >}}) -- they weren't installed


[{{< imgproc src="trisect_sample_left.png" cmd="Fit" opt="1024x768" >}}
{{< /imgproc >}}](trisect_sample_left.png)

[{{< imgproc src="trisect_sample_right.png" cmd="Fit" opt="1024x768" >}}
{{< /imgproc >}}](trisect_sample_right.png)

[{{< imgproc src="trisect_sample_color.png" cmd="Fit" opt="1024x768" >}}
{{< /imgproc >}}](trisect_sample_color.png)
