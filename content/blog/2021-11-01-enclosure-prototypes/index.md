---
title: "Sexton Enclosures are Here!"
linkTitle: "Enclosures!"
date: 2021-11-02
description: "The first two Trisect pressure vessels have arrived!"
---

The first two Trisect pressure vessels have arrived from [Sexton](https://thesextonco.com/)!

{{< imgproc src="trisect_in_enclosure.jpg" cmd="Fit" opt="1024x768" >}}
{{< /imgproc >}}

Working on getting this unit booted and we can move to focusing!
