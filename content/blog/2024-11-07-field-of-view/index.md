---
title: "Increasing the Trisect Field of View"
date: 2024-11-07
tags:
    - trisect
    - optics
resources:
 - src: "*.[jpg|jpeg|png]"
math: true
---

[{{< imgproc src="trisect.jpeg" cmd="Fit" opt="640x480" >}}
{{< /imgproc >}}](trisect.jpeg)

I've been interested in increasing the field of view of the Trisect;  I thought I'd start by measuring the field of view with the current Edumund Optical lenses (shown above).

To take these measurements, I've set up the Trisect with the faceplate **off** (no domes), facing a normal plate.   I've taped two rulers to plate (yes, yes, at some point I'll get some precision graph paper).   The distance from Trisect to lens is discussed below;  In general I've measured from the **flange** of the lens as we can calculate the entrance pupil from that.

As a reminder, for a pinhole camera, the horizontal field of view is

\[
FOV_H = 2 \tan^{-1}\left(\frac{H}{2V}\right)
\]

where FOVH is the horizontal field of view, H is the total edge-to-edge view span of the target, and V is the distance from the camera center to the target.  Since this is just a rough order-of-magnitude comparison, I'm only measuring horizontal FOV; and I'm showing the original distorted images so I can assess the level of lens distortion.

# Current Stock Lens: Edmund Optics

The current "stock" lens on all three cameras is an [Edmund Optics 4mm e.f.l. f/4 UCi lens](https://www.edmundoptics.com/p/4mm-f-4-uci-series-fixed-focal-length-lens/38748/).   It is very high quality but bulky.

All images are taken with the target 2 inches / 51mm from the flange of the lens on the **mono** cameras.    The lens on the color camera sits (by eye) 5mm **closer** to the target when in focus.  From the spec sheet, the entrance pupil distance is
12.69mm, which is added to the distance *V*

[{{< imgproc src="eo_4mm_left.jpeg" cmd="Fit" opt="320x600" >}}Left.  122mm span.
{{< /imgproc >}}](eo_4mm_left.jpeg)
[{{< imgproc src="eo_4mm_color.jpeg" cmd="Fit" opt="320x600" >}}Color.  95mm span.
{{< /imgproc >}}](eo_4mm_color.jpeg)
[{{< imgproc src="eo_4mm_right.jpeg" cmd="Fit" opt="320x600" >}}Right.  120mm span.
{{< /imgproc >}}](eo_4mm_right.jpeg)

This gives:

| Left | Color | Right |
|------|-------|-------|
|  88.4 deg | 74.3  |  87.5 |

These numbers are fairly consistent, with the color having a slightly smaller span as the lens is closer to the target (if we use a distance-to-object of 45mm instead of 50, we get 78.9 d).  Note the mono cameras show a slightly *wider* HFOV than the EO spec, which quotes 77.8 for a 1/2.5" sensor and 84.8 for a 1/2" sensor (both sensors on Trisect are nominally 1/2.3")

The IXM477 is

> (1.55um * 4072 pixels) = 6.31mm wide

while the IMX392 is

> (3.45um * 1936 pixels) = 6.68mm wide

so I would expect a slightly wider FOV from the mono cameras in any case.

However, given the accuracy of the distance-to-object measurement (poor), that's Ok for a ballpark measurement.

The color image is also more severely cropped from the full sensor size (4072 sensor -> 3840 image) than the mono (1936 -> 1920).

----

[{{< imgproc src="trisect_commonlands.jpeg" cmd="Fit" opt="640x480" >}}
{{< /imgproc >}}](trisect_commonlands.jpeg)

## Commonlands lenses

For comparison, I bought a couple of [Commonlands M12 lenses](https://commonlands.com/collections/m12-lenses).   I ... might not fully understand minimum focusing distance, a few of the wider lenses I can't get to focus at short distances.   But here are a few comparisons.   All tests used the left (or color) camera with the target at the same distance as above.   I _did_ need to move the target out of the way when swapping lenses so distance-to-target is only approximate (within +/- 1mm?).

One of the lenses wouldn't focus on the Allied Vision mono camera but would on the color camera, I've included it for comparison:

| Lens | EFL (nom) | Span (mm) | FOV (mono) | FOV (color) |
|------|-----|----|----|------|
| [CIL034](https://commonlands.com/products/ip67-low-distortion-m12-lens-cil034) | 3.2mm | 175mm | 120deg | - |
| [CIL027](https://commonlands.com/products/low-distortion-m12-lens-cil027) | 2.7mm | 220mm (est) | 130deg (est) | - |
| [CIL023](https://commonlands.com/products/wide-angle-no-distortion-m12lens) | 2.2mm | 250mm | - | 135deg (est) |

### CIL034

[{{< imgproc src="CIL034_left.jpeg" cmd="Fit" opt="320x600" >}}
{{< /imgproc >}}](CIL034_left.jpeg)

### CIL027

[{{< imgproc src="CIL027_left.jpeg" cmd="Fit" opt="320x600" >}}
{{< /imgproc >}}](CIL027_left.jpeg)

### CIL023

With the color camera

[{{< imgproc src="CIL023_color.jpeg" cmd="Fit" opt="320x600" >}}
{{< /imgproc >}}](CIL023_color.jpeg)

Wow.   That's a much broader FOV.  IQ is good and there's less distortion!  (I should note this was a hasty focus for testing).   I still can't figure out why the color balance from Argus is so wonky.

I do need to check the depth of field, the CIL027 is a f/2.7 lens while the CIL034 is available at f/2.3, 2.7 and 4.2.  While more light is better, I want to ensure we get a reasonable working depth in the water.

On the plus side, the Commonlands lenses are significantly smaller and may justify a redesigned front plate which is much shallower.  Given we would need to redesign the housing for Orin anyway, might kill two birds with one stone.

[{{< imgproc src="featured_image.jpg" cmd="Fit" opt="640x480" >}}
(This image is from previous focusing, but shows the test rig.  High tech!)
{{< /imgproc >}}](featured_image.jpg)
