---
title: "Documentation in flux"
date: 2024-10-28
tags:
    - drysect
    - orin
    - bagsful
    - crowd
resources:
 - src: "*.jpg"
---

As I go through the process of upgrading Trisect to [Jetpack 5.1.4](), there will be some flux in the documentation.  Please [email me](mailto:amarburg@uw.edu) with questions or post an issue to [trisect_sw](https://gitlab.com/rsa-perception-sensor/trisect_sw/-/issues).
