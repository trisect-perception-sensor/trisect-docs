---
title: "Trisect \"Dognight\", Jetpack 6.1, and Orin NX \"Super\""
date: 2025-01-05
tags:
    - trisect
    - dognight
resources:
 - src: "*.[jpg|jpeg|png]"
math: true
---

[![](jetson-orin-tech-blog-nano-super-jhh-special-1920x1080-1.gif)](https://developer.nvidia.com/blog/nvidia-jetson-orin-nano-developer-kit-gets-a-super-boost/)

*GIF from Nvidia*

NVidia recently announced the [Nano Super](https://developer.nvidia.com/blog/nvidia-jetson-orin-nano-developer-kit-gets-a-super-boost/), promising a huge boost in ML processing power.  Surprisingly, the "Super"-ness is actually a **software** upgrade, and [I'm already testing our Nano in "Super" mode]().   Buried in the press release, they mention they will _also_ [increase the GPU performance](https://developer.nvidia.com/blog/nvidia-jetson-orin-nano-developer-kit-gets-a-super-boost/#all_jetson_orin_nano_series_and_jetson_orin_nx_series_modules_gets_a_super_performance_boost%C2%A0) of the Orin NX.  Woo!

As of right now, those updated modes do not appear to be available (maybe Jetpack 6.1.1?), _but_ to take advantage of them, the Trisect software needs to be rebased to Jetpack 6.x.  I had previously avoided the jump from Jetpack 5-to-6 because the Xavier modules in the existing Trisect heads are not supported by Jetpack 6.x -- so there were no benefits for the sensors we have in the water -- and because Jetpack 6 switches to the new Out-of-Tree (OOT) module build system.  While OOT isn't necessarily any harder than the existing build system, it's another technical hill to climb to keep the Trisect up to date.

Luckily, we had decided to use the Jetson Nano on [another project](https://depts.washington.edu/uwaplopal/post/2024-11-20-vc-cameras-jetson-nano.html), providing a good opportunity to become familiar with the OOT build system.  In that case, I actually started with the [Allied Vision toolchain for Jetpack 6](https://github.com/alliedvision/alvium-jetson-driver-release) and re-engineered it to build OOT modules for [Vision Components](https://www.vision-components.com/en/) cameras, so I was able to both get familiar with Jetpack 6 and with Allied's system for building kernel modules for OOT.

To bring it all full circle, I've upgraded our Drysect (Orin NX 16GB with two Alvium cameras) to Jetpack 6.1, and posted [our version of the AVT kernal modules](https://github.com/apl-ocean-engineering/alvium-jetson-driver-release/tree/l4t-36.4.0/alpha).   At this point it boots.  Development continues, while I wait for the release of the "Super" mode for the Orin NX.

To mark this next generation of Trisect software, we've christened the [fourth Trisect release, `Dognight`]({{< ref "Versions.md" >}}), which is tied to Jetpack 6.1.x on the Orin NX.  More details as I make progress.
