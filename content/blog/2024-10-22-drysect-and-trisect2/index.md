---
title: "Jetpack 5.1.4 on Drysect and Trisect2"
date: 2024-10-22
tags:
    - drysect
    - orin
    - bagsful
    - crowd
resources:
 - src: "*.jpg"
---

It's been a while, but I finally had a chance to re-engage with the Orin NX Drysect.  I also have the original `trisect2` available for an upgrade.

After a bit of work, I've upgraded both to [Jetpack 5.1.4](https://developer.nvidia.com/embedded/jetpack-sdk-514) / [L4T-35.6.0](https://developer.nvidia.com/embedded/jetson-linux-r3560).   My customized version of the Allied Vision kernel is [here](https://github.com/amarburg/linux_nvidia_jetson/tree/trisect-l4t-35.6.0).   Now that there is a mainstream IMX477 driver, the differences from the upstream [Allied Vision kernel](https://github.com/alliedvision/linux_nvidia_jetson) as minor, primarily changes in the device trees.

- Upgrade from L4T-35.5.0 to L4T-35.6.0 (Allied Vision has not released a kernel package for 35.6.0)
- A new option to invert the GPIO reset line for the IMX477.   Due to N-MOSFET on the flex circuit, the logic of the IMX477 reset is inverted in Trisect (0 for **ON**, 1 for **OFF**).   The GPIO logic is hardcoded in the Allied Vision kernel.

I still can't flash directly to my built kernel.  As detailed [in the docs]({{< ref "/docs/Software_Bagsful" >}}) I am installing the stock kernel with SDK manager, then manually upgrading the kernel in userspace (for both platforms).

I believe Jetpack 5.x is the last supported version for the Xavier, so this should be the final kernel upgrade on the original Trisect.   As such, this represents a new version:

* Xavier-based Trisect release "Bagsful"
* Orin-based Trisect release "Crowd"

I am continuing to work on improvements to the userspace software.
