---
title: "Testing Trisect at OSU"
summary: "Preliminary field testing with the Trisect"
date: 2022-08-12
resource:
 - src: "*.png"
---

In August, we took the Trisect to OSU to assist with [testing in the Hinsdale Directional Wave Basin.](https://depts.washington.edu/uwaplopal/post/2022-08-12-osu-hinsdale.html)

This was the first public outing for the [minimum viable product]({{< relref "2022-08-01-mvp-sample-data" >}}) of the [trisect_sw stack]({{< ref "Software_User" >}}).

The images shown here were collected by Tim Player @ OSU.

[{{< imgproc src="hinsdale_left.png" cmd="Fit" opt="1024x768" >}}
{{< /imgproc >}}](hinsdale_left.png)

[{{< imgproc src="hinsdale_right.png" cmd="Fit" opt="1024x768" >}}
{{< /imgproc >}}](hinsdale_right.png)
