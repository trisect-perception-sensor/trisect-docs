---
title: "First stereo on Drysect"
summary: "Preliminary stereo on Drysect"
date: 2023-10-01
tags:
    - drysect
    - vpi
    - orin
featured_image: left.jpg
resources:
 - src: "*.jpg"
---

After a bit of hacking, I've reached stereo MWE with the [Orin-NX-based Drysect]({{< relref "2023-09-22-behold-the-drysect" >}}) using [VPI 2.3](https://docs.nvidia.com/vpi/).  Changes to [gpu_stereo_image_proc_vpi](https://github.com/apl-ocean-engineering/gpu_stereo_image_proc/tree/devel/orin_nx_drysect/gpu_stereo_image_proc_vpi) were actually pretty minimal, there were a few more adaptations required in [trisect_camera_driver](https://gitlab.com/rsa-perception-sensor/trisect_camera_driver) and [libtrisect](https://gitlab.com/rsa-perception-sensor/libtrisect), including adding a few features (software triggering) and some refactoring.

[{{< imgproc src="left.jpg" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}](left.jpg)

[{{< imgproc src="disparity.jpg" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}](disparity.jpg)

Overall results are good-not-spectacular, but it relieves my worry that the black-box VPI algorithm might not be as good as the black box Visionworks algorithm.

There's sure to be better performance with more VPI tuning.  Also, [gpu_stereo_image_proc_vpi](https://github.com/apl-ocean-engineering/gpu_stereo_image_proc/tree/devel/orin_nx_drysect/gpu_stereo_image_proc_vpi) doesn't currently implement the disparity image smoothing / filtering found in [gpu_stereo_image_proc_visionworks](https://github.com/apl-ocean-engineering/gpu_stereo_image_proc/tree/devel/orin_nx_drysect/gpu_stereo_image_proc_visionworks), which will also improve image quality.

And the current focus/calibration were done in haste.
