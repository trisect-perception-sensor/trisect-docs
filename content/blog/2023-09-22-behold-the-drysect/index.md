---
title: "Initial bring-up of Drysect"
summary: "Details the initial configuration and bring-up of an Orin NX for developing the next-generation Trisect"
date: 2023-09-22
tags:
    - drysect
    - orin
resources:
 - src: "*.png"
---

As I mentioned briefly in the [previous post]({{< relref "2023-09-14-seeed-j401-dual-imx477" >}}), a few factors recently came together which lead to building an Orin NX-based benchtop dev system which hints at the future hardware configuration for the Trisect.

The biggest factor was not having any Trisect spare parts available, plus the relative age of the current Trisect Jetpack/L4T installation (Jetpack 4.4.x / L4T-32.4.4).  I felt stuck on that revision due to the effort in combining the AVT and Ridgerun kernel patches to support the Alvium and Pi HQ cameras simultaneously, and I couldn't take one of the Trisects offline for a long time to test newer Jetpacks (or risk bricking a Trisect).   Being at this older Jetpack meant Ubuntu 18.04, which meant ROS melodic.

I noted that the more recent L4T kernels contained support for IMX477 sensors out of the box so I hoped that future AVT+IMX477 kernels would be less painful.

I was also tracking the transition from [Visionworks](https://forums.developer.nvidia.com/t/visionworks-will-be-retired-after-jetpack-4-6-x/170397) to [VPI](https://developer.nvidia.com/embedded/vpi).  Jetpack 4.4.1 contains the relatively ancient VPI 0.4, so it was not a great platform for forward-porting [gpu_stereo_image_proc](https://github.com/apl-ocean-engineering/gpu_stereo_image_proc) to use VPI.

And on top of all that, I wanted a development set of Alvium cameras available for testing some new [wider-angle lenses.](https://commonlands.com)

Given the [Orin NX](https://developer.download.nvidia.com/assets/embedded/secure/jetson/orin_nx/docs/Jetson_Orin_NX_DS-10712-001_v0.5.pdf?PELNaPoBswHC1YiYJs4wwV8CMIHjuCzyDmynEvcyHc4WxA0IdxCUL33cLFnU9hFa5WFFWG9h3a57DbVGuQIL6Iw78dMG9a1fVcNtTlH86Il9rM7BJ5k66-K6T9gqDIlf4wWZwLJB5y69x_6O1jYKYn62n2OIcLNBNgatcjg8UbLVUwlxp9Js1oo0Miqo8Q==&t=eyJscyI6ImdzZW8iLCJsc2QiOiJodHRwczovL3d3dy5nb29nbGUuY29tLyJ9) is a mostly-drop-in replacement for the Xavier NX, it seemed like an obvious choice as a development platform.

So, in sum, I wanted to build a new Trisect which would let me:

- Play with an Orin NX
- Use a more current Jetpack
- Upgrade to Ubuntu 20.04 / ROS `noetic`
- Work with a more recent VPI
- Have a spare development platform
- Try some new lenses


And so, behold the Drysect:

[{{< imgproc src="drysect.jpg" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}](drysect.jpg)

The cameras are the same as the [current Trisect spec]({{< relref "Bill_of_Materials" >}}).  The mainboard is a Seeed Studio [J401 baseboard](https://www.seeedstudio.com/reComputer-J401-Carrier-Board-for-Jetson-Orin-NX-Orin-Nano-p-5636.html?queryID=84c3e5066ce025ac295f33f63d7a2fdb&objectID=5636&indexName=bazaar_retailer_products) with an Orin NX 16GB.   The J401 is very similar to the Xavier/Nano NX development kit, and by default boots with kernels/device tree entries for that board -- it doesn't require any `dtb` customization (to boot).

Like the official dev board, it has two camera connectors, though [as discussed previously]({{< relref "2023-09-14-seeed-j401-dual-imx477" >}}) there are some differences in how the module's CSI lanes map to the connectors.   This baseboard uses the "standard" 15-pin Pi Camera connector, so I was able to use the [Allied Vision 14918](https://cdn.alliedvision.com/fileadmin/content/documents/products/accessories/embedded/user-guide/Jetson-Nano-Xavier-NX-Dev-Kit_Adapter_User-Guide.pdf) to connect the Alvium cameras, while the Pi HQ connects directly.

Since there are only two connectors, I can only test two cameras at a time.

---

Adapting the latest [Allied Vision kernel](https://github.com/alliedvision/linux_nvidia_jetson) required a few very minor modification, plus adding appropriate DTBOs for the J401 camera in various configurations.    See [this "devel" branch](https://github.com/amarburg/linux_nvidia_jetson/tree/devel/l4t-35.4.1-drysect-testing) for my bleeding edge kernel and DTBOs.   I have both IMX477+Alvium and dual-Avium configurations "working" (I can enumerate both cameras and get data from them), although I haven't finished the plumbing to actually see images from the Alvium cameras.  I've also tried dual-IMX477 and I believe it works, but didn't spend much time on it.

So far the Orin NX has been straightforward to deal with.   Flashing (and re-flashing) with SDK manager has been mostly painless.

The plan from here is to update [`libtrisect`](https://gitlab.com/rsa-perception-sensor/libtrisect) and [`trisect_camera_driver`](https://gitlab.com/rsa-perception-sensor/trisect_camera_driver) working towards getting images streaming from the cameras.

Once there I'll calibrate the new test lenses and start forward-porting [`gpu_stereo_image_proc_vpi`](https://github.com/apl-ocean-engineering/gpu_stereo_image_proc/tree/trisect-dev/gpu_stereo_image_proc_vpi) to the latest version of VPI.   Stereo will be a little iffy since I can only software-synchronize the cameras, but will be fine for static testing.
