---
title: "RAFT-Stereo on Drysect/Trisect"
date: 2024-10-29
tags:
    - drysect
    - orin
    - bagsful
    - crowd
    - raft
    - stereo
resources:
 - src: "*.[jpg|png]"
---

[{{< imgproc src="featured_image.jpg" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}](featured_image.jpg)


* OPAL [RAFT_stereo ROS1 node in Gitlab](https://gitlab.com/apl-ocean-engineering/raft_stereo)


Recently, we've been testing [RAFT-Stereo](https://github.com/princeton-vl/RAFT-Stereo) as a high-performance, turnkey stereo depth algorithm.  Up to this point, we've been running our [ROS1 node](https://gitlab.com/apl-ocean-engineering/raft_stereo) on the desktop, receiving the left/right image streams over the network as ROS topics from the Trisect.

I wanted to evaluate performance when running on both generations of Jetson.

I initially had some issues with PyTorch - CUDA versioning and similar, but I think I've found a relatively smooth install path; those are included [below]({{< ref "#install" >}}).   To start with the interesting bits, here are some benchmarks.

## Benchmarks

All platforms are running [`camera_only.launch`](https://gitlab.com/rsa-perception-sensor/trisect_sw/-/blob/trisect-5.1.4/release-2.x/launch/camera_only.launch?ref_type=heads) from [`trisect-5.1.4/release-2.x`](https://gitlab.com/rsa-perception-sensor/trisect_sw), so undistortion, rectification and downsampling are done using VPI before the images are published to ROS.  RAFT is provided with 960x608 greyscale images (downsampled by a factor of two) at 10FPS.   The color camera is not running on the Trisect (`do_color:=false`).

Here's the bottom line.  FPS are averaged over a 30 second window.   Using the default `raft_stereo` settings (noted below):

| | RTX 4080 | Drysect | Drysect | Trisect2 |
|-|----------|---------|---------|----------|
| Module |          | Orin NX | Orin NX | Xavier NX |
| NVP Mode |          | MAXN    | 25W | 20W 6Core (w/ jetson_clocks) |
| FPS | (tbd, but anecdotally, near frame rate of 10Hz) | 3.66  |  1.83  | 2.177 |

Which is ... fairly workable.   The Orin NX has a more powerful GPU on paper but the ~408MHz GPU clock in the 25W mode throttles throughput.  The MAXN mode represents the absolute performance ceiling on the Orin NX.

Just for reference, here are the NVP modes for the Orin NX 16GB, [from Ridgerun](https://developer.ridgerun.com/wiki/index.php/NVIDIA_Jetson_Orin_NX/JetPack_5.1.1/Performance_Tuning/Tuning_Power#Max-Q_and_Max-P_Power_Efficiency):

{{< imgproc src="orin_nx_nvp_modes.png" cmd="Fit" opt="480x480" >}}
{{< /imgproc >}}


Running RAFT directly on device may only fit a limited set of use cases, but is certainly usable.    More work on optimization (and evaluating accuracy) to come.

I'll note the upgrade to Jetpack-5.1.4 broke a few things in [gpu_stereo_image_proc_vpi](https://github.com/apl-ocean-engineering/gpu_stereo_image_proc/tree/devel/orin_nx_drysect/gpu_stereo_image_proc_vpi) so RAFT (or offboard) is the only stereo option currently available.  I plan to fix the VPI implementation, but given the performance of block matching vs RAFT, I think block matching's days are numbered...   but it's an important point of comparison.

[{{< imgproc src="raft_triplet.png" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}](raft_triplet.png)

*Left, disparity, and right images from Drysect.  Drysect is sitting on a table so the foreground / lower half of the image is unreliable.
(Note, the left camera is quite out of focus ....)*


## Install Instructions {#install}

This applies to both the Orin NX and Xavier NX, both running [Jetpack 5.1.4](https://docs.nvidia.com/jetson/archives/jetpack-archived/jetpack-514/release-notes/index.html).   It assumes a [working ROS1 install,]({{< ref "docs/Software_Bagsful/Clean_Install_5.1.4#ros_install" >}}) and a complete Jetpack install with CUDA, etc.

For these installs, I am using [rosvenv](https://github.com/ARoefer/rosvenv).  I am not sure if it is essential, but it works _with_ it, and I'm interested in whether it solves other future issues.

1. Install Pytorch per [NVidia's instructions](https://docs.nvidia.com/deeplearning/frameworks/install-pytorch-jetson-platform/index.html).  First Install system dependecies:

```shell
$ sudo apt-get update && sudo apt-get -y  python3-pip libopenblas-dev;
```

2. Install [cuSparseLT](https://docs.nvidia.com/cuda/cusparselt/index.html) system-wide:

```shell
$ cd /tmp
$ wget https://developer.download.nvidia.com/compute/cusparselt/redist/libcusparse_lt/linux-aarch64/libcusparse_lt-linux-aarch64-0.6.3.2-archive.tar.xz
$ tar xf libcusparse_lt-linux-aarch64-0.6.3.2-archive.tar.xz
$ cd libcusparse_lt-linux-aarch64-0.6.3.2-archive
$ sudo cp -a include/* /usr/local/cuda/include/
$ sudo cp -a lib/* /usr/local/cuda/lib64/
$ cd .. && rm -rf libcusparse_lt-linux-aarch64-0.6.3.2-archive
```

3. Install the `pytorch` wheel from Nvidia system-wide:

```shell
$ cd /tmp
$ wget https://developer.download.nvidia.cn/compute/redist/jp/v512/pytorch/torch-2.1.0a0+41361538.nv23.06-cp38-cp38-linux_aarch64.whl
$ sudo pip install  torch-2.1.0a0+41361538.nv23.06-cp38-cp38-linux_aarch64.whl
```

4. Use rosvenv to create a new ROS-venv workspace (assumes you have rosvenv installed):

```shell
$ cd ~ && createROSWS raft_ws
```

5. Install RAFT-stereo:

```shell
$ cd src
$ git clone https://gitlab.com/apl-ocean-engineering/raft_stereo
```

On a standard ROS system, the APT package can be installed `ros-noetic-ros-numpy`.   On Trisect's custom ROS distro, this is already installed.

4. Install the custom sampler provided by RAFT-stereo:

```shell
$ cd raft_stereo/src/raft_stereo/sampler

# Should install in the ROS workspace virtual environment created by rosvenv
$ pip install .
```

5. Download the models

```shell
$ cd ..
$ ./download_models.sh
```


6. Catkin build and run as normal:

```shell
$ cd ~/raft_ws
$ catkin build

## Re-activate the venv (similar to 'source devel/setup.sh')
$ activateROS .
$ roslaunch raft_stereo raft_native.launch
```


## RAFT Parameters

All of the benchmarks above used the following `raft_stereo/config.yaml`:

```yaml
model_path: "src/raft_stereo/models/raftstereo-realtime.pth"
mixed_precision: True
num_samples: 10
valid_iters: 7
save_numpy: False
hidden_dims: [128, 128,128]
corr_implementation: reg_cuda
shared_backbone: True
corr_levels: 4
corr_radius: 4
n_downsample: 3
context_norm: batch
slow_fast_gru: True
n_gru_layers: 2
device: 0
```
