---
title: "OS and Kernel (Amigos)"
linkTitle: "OS/Kernel (Amigos)"
weight: 60
description: >
  Trisect Software.
---

Trisect "Amigos" for the Xavier NX runs [Jetpack 4.5.1](https://developer.nvidia.com/jetpack-sdk-451-archive) / [L4T-32.5.1](https://developer.nvidia.com/embedded/linux-tegra-r3251).
