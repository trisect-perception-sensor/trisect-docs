---
title: "Kernel"
linkTitle: "Linux Kernel"
weight: 40
description: >
    Details on the Trisect Linux Kernel.
---

Trisect uses a custom Linux kernel ([on Github](https://github.com/amarburg/linux_nvidia_jetson)).   The current version targets Jetpack 4.4.2 and is based on the [Allied Vision Alvium Linux Kernel](https://github.com/alliedvision/linux_nvidia_jetson) with the patches for the [Antmicro Nano Baseboard](https://github.com/antmicro/jetson-nano-baseboard/tree/master/linux-patches) installed.  It also includes portions of [Ridgerun's Jetson support for the IMX477](https://github.com/RidgeRun/NVIDIA-Jetson-IMX477-RPIV3).

The customizations to the kernel cover a number of tasks:

* Include support for the Alvium and IMX477 cameras (from Allied Vision and Ridgerun, resp).
* Include a device tree customized for Trisect.
* Reduced the extraneous modules compiled for the kernel to reduce module tree size.


# Building the Kernel

We adopt Allied Vision's technique of including the full kernel source in the repo, along with scripts which support essential dev tasks.

1. Check out the repository:

```
git clone https://github.com/amarburg/linux_nvidia_jetson
cd linux_nvidia_jetson
```

1. Prepare the build directory:

```
cd avt_build
./setup.sh build nx
```

Which *build* is the local build directory.

1. Build the kernel, modules and device tree.

```
./build.sh build nx all all
```

1. At this point, there are multiple options for deploying the kernel (and admitting I'm still figuring this out myself).  The simplest is to make a deployment tarball:

```
./deploy.sh build nx tarball
```

Which will make a tarball containing the kernel and module images.  Copy this to the Jetson, untar, and run the `install.sh` contained within.   *I should not I've found this approach can samage the redundant boot images set up by the NVidia software, still trying to figure out why it's not safe.*

If the Jetson is in recovery mode and you have access to the USB interface,  you can also flash it from your host computer:

```
./deploy.sh build nx flash-kernel
```

```
./deploy.sh build nx flash-dtb
```

There's also `flash-all` which will attempt to flash the filesystem as well.   I have yet to figure out an option for flashing the kernel and dtb, but not the filesystem.   After flashing, it will reboot the Jetson.
