---
title: "OS and Kernel (Bagsful/Crowd)"
linkTitle: "OS/Kernel (Bagsful/Crowd)"
weight: 30
---

Trisect "Bagsful (Xavier NX) and "Crowd" (Orin NX) are based on [Jetpack 5.1.4](https://developer.nvidia.com/embedded/jetpack-sdk-514) / [L4T-35.6.0](https://developer.nvidia.com/embedded/jetson-linux-r3560).
