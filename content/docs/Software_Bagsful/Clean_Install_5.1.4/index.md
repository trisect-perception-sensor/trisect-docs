---
title: "Clean install"
weight: 50
description: >
    The steps required to create a Trisect from scratch
resources:
 - src: "*.jpg"
---

> **Warning:** This is a work-in-progress as I work through the upgrade to 5.1.4.

> **Note:** These instructions cover both the Xavier NX-based "Bagsful" and the newer Orin NX-based "Crowd".   Differences are noted below.

The setup process spans three major tasks:

1. Preliminary setup of a board using NVidia SDK Manager.  This installs the preliminary kernel and root filesystem.   It also sets up the initial user and installs core NVidia libraries (CUDA, VPI, etc).
2. Run ansible scripts from [trisect_bootstrap](https://gitlab.com/rsa-perception-sensor/trisect_bootstrap/-/tree/jetpack_5.1.4?ref_type=heads) to install basic dependneices.
3. Install the custom Trisect kernel and DTBs.
4. Installation of application software:
   1. Rebuilding ROS to use the NVidia OpenCV libraries
   2. Instalaltion of [Trisect software stack.](https://gitlab.com/rsa-perception-sensor/trisect_sw)

These steps are still fairly manual; it's acceptable for the small number of Trisect units.  I try to concentrate the repeatable tasks in the Ansible scripts;  the ROS install technically falls within that camp but is time consuming and should only be required very infrequently.

These steps are detailed below:

## 1. Preliminary set up with SDK Manager

For now we use SDK manager for initial setup of the board ... this leads to installation of **more** packages than strictly required, some are removed by the Ansible scripts ... but is acceptable for now.  Bootstrapping directly from the Allied vision kernel tree didn't work reliably, and it wasn't worth debugging.

Install and run. the latest [SDK Manager](https://developer.nvidia.com/sdk-manager).

Install the USB-C update cable, and optionally the serial console cable (micro-USB on the Antmicro Nano baseboard) and a monitor.

> The Xavier NX development kit modules require an SD card to boot.   SDK Manager will install the OS on the NVMe, but an SD card is still required.

> **On the Antmicro baseboard:** Press and hold the recovery button (closest to the power plug) while pressing the reset button (second from the power plug).
>
> **On the Seed J401 baseboard:** Install the recovery jumper between pins 2 and 3 and power cycle the Jetson.

The SDK Manager should recognize the board:

[{{< imgproc src="sdk_manager.png" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}](sdk_manager.png)

Select "Jetpack 5.1.4" and start the install.

When prompted, select "Manual Setup" as the unit is already in recovery mode.   Then provide the initial user "trisect" with the default password:

[{{< imgproc src="sdk_manager_connection.png" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}](sdk_manager_connection.png)

On **either platform** select **"NVMe".**

SDK Manager will now flash the unit and install the root filesystem to the NVMe.

Eventually it will prompt for login info to install additional SDK components:

[{{< imgproc src="sdk_manager_install_sdk_components.png" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}](sdk_manager_install_sdk_components.png)

I found the USB network connection was not bulletproof and connecting over ethernet was more reliable -- this requires looking up the unit's IP address (from your router, etc) and providing the username and password from above.

When complete it, SDK manager will provide a summary then quit.

You can then SSH into the unit, or power it down and remove the recovery jumper.

## 2. Run Ansible scripts

[trisect_bootstrap](https://gitlab.com/rsa-perception-sensor/trisect_bootstrap/-/tree/jetpack_5.1.4?ref_type=heads) is designed to be run _on the trisect_ (rather than remotely).

### Bootstrap ansible

To get started, the `ansible_bootstrap.sh` script will install ansible and run the script for the first time.  Part of that install will put a copy of `trisect_bootstrap` on the drive.  Subsuquent runs can use that checkout.

The bootstrap scripts requires a configuration file `~/.trisect_bootstrap.sh` which contains the following environment variables:

```
export TRISECT_HOSTNAME=<hostname>
```

Then,

```
wget -O- https://gitlab.com/rsa-perception-sensor/trisect_bootstrap/-/raw/jetpack_5.1.4/ansible_bootstrap.sh | /usr/bin/bash
```


### Running ansible manually

To run the ansible manually, checkout out the git repo and use `ansible-playbook`

```
source ~/.trisect_bootstraps.sh
git clone -b orin_nx https://gitlab.com/rsa-perception-sensor/trisect_bootstrap.git
cd trisect_bootstrap
ansible-playbook -vvv --extra-vars "var_hostname=$TRISECT_HOSTNAME" ansible/trisect.yml
```


## 3. Install the Trisect kernel.


## 4.  Install ROS from source {#install_ros}


## 5. Install Trisect S/w

Install triggerd
