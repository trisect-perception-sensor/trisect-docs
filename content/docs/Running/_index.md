---
title: "Using Trisect"
linkTitle: "Using Trisect"
weight: 10
description: >
  Operation of the Trisect.
resource:
 - src: "*.jpg"
---

We provie a basic [ROS-based software stack]({{< relref "ROS_Software" >}}) called `trisect_sw` which publishes camera streams from all three cameras, plus stereo-generated disparity.  In general, we don't run this as an always-on system-level service -- though it certainly could be set up that way .  Instead, we assume different users will have different needs (or different sets of software configurations and parameters) each with their own copy of the software.  There may be multiple users, and multiple copies of the software on a single Trisect, some with different application-specific modifications.

We typically have a standard system-wide user `trisect` which is the main user on the platform.  Individual users may access the `trisect` account via SSH to start the `trisect_sw` stack.   However, personalized accounts could be added to the Trisect as needed, as long there is a firm policy on how to share the hardware between users.
