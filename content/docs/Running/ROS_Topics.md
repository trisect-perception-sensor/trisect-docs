---
title: "ROS Topics"
weight: 30
description: >
  Descriptions of the ROS topics provided by the standard software stack.
resource:
 - src: "*.jpg"
---

This page is now up-to-date for the [Jetpack-5.1.4/release-2.x](https://gitlab.com/rsa-perception-sensor/trisect_sw)

Internally, the stereo processing code downsampled (shrinks) the incoming images by the factor `stereo_downsample` as provided at launch (defaults to 2).

## Black and white camera topics

| Topic                       | ROS Message Type                                                                                                                             | Description                                                                             |
| --------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| `/trisect/left/image_raw`           | [`sensor_msgs::Image`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html)                                                    | Left camera imagery                                                                     |
| `/trisect/left/image_raw/compressed`           | [`sensor_msgs::CompressedImage`](http://docs.ros.org/en/melodic/api/sensor_msgs/html/msg/CompressedImage.html)                                                    | *Compressed* Left camera imagery                                                                     |
| `/trisect/left/camera_info`         | [`sensor_msgs::CameraInfo`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CameraInfo.html)                                          | Intrinsic calibration info for the left camera.                                         |
| `/trisect/left/imaging_metadata`    | [`trisect_msgs::TrisectImagingMetadata`](https://gitlab.com/rsa-perception-sensor/trisect_msgs/-/blob/master/msg/TrisectImagingMetadata.msg) | Custom message containing current exposure and gain settings of left camera.             |
| `/trisect/right/image_raw`          | [`sensor_msgs::Image`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html)                                                    | Right camera imagery                                                                    |
| `/trisect/right/image_raw/compressed`           | [`sensor_msgs::CompressedImage`](http://docs.ros.org/en/melodic/api/sensor_msgs/html/msg/CompressedImage.html)                                                    | *Compressed* Right camera imagery                                                                     |
| `/trisect/right/camera_info`        | [`sensor_msgs::CameraInfo`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CameraInfo.html)                                          | Intrinsic calibration info for the right camera.                                        |
| `/trisect/right/imaging_metadata`   | [`trisect_msgs::TrisectImagingMetadata`](https://gitlab.com/rsa-perception-sensor/trisect_msgs/-/blob/master/msg/TrisectImagingMetadata.msg) | Custom message containing current exposure and gain settings of right camera.            |

## Color camera topics

| Topic                | ROS Message Type                                                                                    | Description                                      |
| -------------------- | --------------------------------------------------------------------------------------------------- | ------------------------------------------------ |
| `/trisect/color/image_raw`   | [`sensor_msgs::Image`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html)           | Color camera imagery                             |
| `/trisect/color/image_raw/compressed`   | [`sensor_msgs::CompressedImage`](http://docs.ros.org/en/melodic/api/sensor_msgs/html/msg/CompressedImage.html)           | *Compressed* color camera imagery                             |
| `/trisect/color/camera_info` | [`sensor_msgs::CameraInfo`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CameraInfo.html) | Intrinsic calibration info for the color camera. |

## Rectified images

Rectified image topics.  These are an intermediate product of the stereo processing.

| Topic                      | ROS Message Type                                                                          | Description                                                                   |
| -------------------------- | ----------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------- |
| `/trisect/left/image_rect`         | [`sensor_msgs::Image`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html) | Rectified left camera image                                                   |
| `/trisect/left/image_rect/compressed`         | [`sensor_msgs::CompressedImage`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CompressedImage.html) | *Compressed* rectified left camera image                                                   |
| `/trisect/right/image_rect`        | [`sensor_msgs::Image`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html) | Rectified right camera image                                                  |
| `/trisect/right/image_rect/compressed`        | [`sensor_msgs::CompressedImage`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CompressedImage.html) | *Compressed* rectified right camera image                                                  |

Scaled (downsampled) rectified images.  These are the input to stereo processing:

| Topic                      | ROS Message Type                                                                          | Description                                                                   |
| -------------------------- | ----------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------- |
| `/trisect/stereo/downsampled/left/image_rect`  | [`sensor_msgs::Image`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html) | Rectified left camera image scaled down by `shrink_scale`.  |
| `/trisect/stereo/downsampled/left/image_rect/compressed`  | [`sensor_msgs::CompressedImage`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CompressedImage.html) | *Compressed*, downsampled, rectified left camera image.  |
| `/trisect/stereo/downsampled/left/camera_info`  | [`sensor_msgs::CameraInfo`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CameraInfo.html) | Intrinsic calibration info for the downsampled left rectified image. |
| `/trisect/stereo/downsampled/right/image_rect`  | [`sensor_msgs::Image`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html) | Rectified right camera image scaled down by `shrink_scale`.  |
| `/trisect/stereo/downsampled/right/image_rect/compressed`  | [`sensor_msgs::CompressedImage`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CompressedImage.html) | *Compressed*, downsampled, rectified right camera image.  |
| `/trisect/stereo/downsampled/right/camera_info`  | [`sensor_msgs::CameraInfo`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CameraInfo.html) | Intrinsic calibration info for the downsampled right rectified image. |



## Disparity/Stereo outputs

Disparity image outputs from `gpu_stereo_image_proc`.

| Topic                     | ROS Message Type                                                                                            | Description                                                                                                                                   |
| ------------------------- | ----------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| `/trisect/downsampled/disparity`       | [`stereo_msgs::DisparityImage`](http://docs.ros.org/en/noetic/api/stereo_msgs/html/msg/DisparityImage.html) | **Scaled** disparity image.  This is the "best" disparity image from the stereo processor including any filtering.                 |
| `/trisect/downsampled/disparity/image` | [`sensor_msgs::Image`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Image.html)                   | Image of the scaled disparity image, generated by [`disparity_visualize`](https://github.com/ActiveIntelligentSystemsLab/disparity_visualize) |
| `/trisect/stereo/disparity/image/compressed` | [`sensor_msgs::CompressedImage`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/CompressedImage.html)                   | *Compressed* version of `/trisect/downsampled/disparity/image` |

## Point cloud outputs

| Topic                 | ROS Message Type                                                                                      | Description                                                                                                                                                                     |
| --------------------- | ----------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `/trisect/stereo/points2`            | [`sensor_msgs::PointCloud2`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/PointCloud2.html) | Point cloud output from the full-sized disparity image `/disparity`                                                                                                             |
| `/trisect/stereo/passthrough/output` | [`sensor_msgs::PointCloud2`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/PointCloud2.html) | Output from the [`pcl_ros::PassThrough` filter](https://docs.ros.org/en/api/pcl_ros/html/classpcl__ros_1_1PassThrough.html).   This is currently derived from `/scaled_points2` |


## Sensor and Trisect Status topics

| Topic                 | ROS Message Type                                                                                      | Description                                                                                                                                                                     |
| --------------------- | ----------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `/trisect/sensors/raw_data`            | [`apl_msgs/RawData`](https://gitlab.com/apl-ocean-engineering/apl_msgs/-/blob/main/msg/RawData.msg) | Raw data from the [Trisect sensor coprocessor.]({{< ref "I2C_Sensors.md" >}}) |
| `/trisect/sensors/imu`            | [`sensor_msgs/Imu`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Imu.html) | Data from the built-in IMU. |
| `/trisect/sensors/magnetometer`   | [`sensor_msgs/MagneticField`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/MagneticField.html) | Data from the built-in magnetometer. |
| `/trisect/sensors/pressure`       | [`sensor_msgs/FluidPressure`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/FluidPressure.html) | Data from built-in internal air pressure sensor. |
| `/trisect/sensors/relative_humidity` | [`sensor_msgs/RelativeHumidity`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/RelativeHumidity.html)| Data from built-in internal relative humidity sensor. |
| `/trisect/sensors/temperature`    | [`sensor_msgs/Temperature`](http://docs.ros.org/en/noetic/api/sensor_msgs/html/msg/Temperature.html) | Data from built-int internal air temperature sensor. |
| `trisect/raw_data`                | [`apl_msgs/RawData`](https://gitlab.com/apl-ocean-engineering/apl_msgs/-/blob/main/msg/RawData.msg) | *??  May disappear.* |
