---
title: "Building trisect_sw"
linktitle: "Building trisect_sw"
weight: 10
description: >
    Building a clean installation of `trisect_sw`.
resource:
 - src: "*.jpg"
---


These instructions describe a clean installation of the `trisect_sw` --- they don't detail all of the steps required to initialize a trisect from a [clean NVidia Jetpack installation]]({{< relref "Software_Bagsful" >}}).

The trisect software follows the design patterns for ROS `catkin`.  The central repository is [trisect_sw](https://gitlab.com/rsa-perception-sensor/trisect_sw).   To build a new copy of the software, create a Catkin workspace and clone a copy of [trisect_sw](https://gitlab.com/rsa-perception-sensor/trisect_sw) into the `src/` directory:

```
mkdir -p trisect_sw/src
cd trisect_src
catkin init
cd src
git clone https://gitlab.com/rsa-perception-sensor/trisect_sw
```

The file [`trisect_sw.rosinstall`](https://gitlab.com/rsa-perception-sensor/trisect_sw/-/blob/main/trisect.rosinstall) lists other Git repo dependencies.   The `vcs` tool can be used to check out all of those repositories.   `rosdep` can be used to install any missing ROS dependencies.

```
vcs import < trisect_sw/trisect_sw.rosinstall
rosdep install --from-paths . --ignore-src -r -y
```

Then build

```
cd ..
catkin build
```
