---
title: "Running trisect_sw"
linktitle: "Running trisect_sw"
weight: 20
description: >
  Running the standard `trisect_sw` stack.
resource:
 - src: "*.jpg"
---

These instructions assume the [trisect_sw stack has been installed]({{< relref "Building_Trisect_sw" >}}) on the system.   Following the pattern we use, we assume the software has been installed by the user `trisect` in the directory `/home/trisect/trisect_sw`.   The software can be installed by other users, or in other locations, or multiple version of the software may be installed -- depending on your particular needs.

This page describes how to run and interact with the Trisect software.   See [the ROS software page]({{< relref "ROS_software" >}}) for a description of the software components and architecture.

## Accessing the trisect

The Trisect is accessed over SSH.  We use a generic user `trisect`, though personal or other system-level accounts can be added for development.

By default the Trisect requests a DHCP address, **and**  has a static IP address of 192.168.88.1.

## Configuring the Jetson power mode



## Running trisect_sw

We suggest running the trisect software stack in a `tmux` session to allow the software to run persistently even if the SSH session drops.  This also allows a different user in a second SSH session to log in and control/stop the software if needed.

```
ssh trisect@(ip address/hostname for trisect)
tmux
```

We install the software in a catkin workspace in `~/trisect_ws` directory:

```
cd ~/trisect_ws
source devel/setup.zsh
```

The trisect software can then be launched using `roslaunch`:

```
roslaunch trisect_sw trisect_nodelet.launch <args>
```

The [`trisect_nodelet.launch`](https://gitlab.com/rsa-perception-sensor/trisect_sw/-/blob/main/launch/trisect_nodelet.launch) file starts the `trisect_sw` stack as a set of [ROS nodelets](http://wiki.ros.org/nodelet) to reduce inter-process communication.  Alternatively, [`trisect_node.launch`](https://gitlab.com/rsa-perception-sensor/trisect_sw/-/blob/main/launch/trisect_node.launch) can be called to launch discrete nodes;  this is less efficient but provides better separation between nodes to debug issues (crashing nodes, etc.).

Once running, Trisect data is available through a range of [ROS Topics]({{< relref "ROS_Topics" >}}).   A number of [non-ROS outputs]({{< relref "Non-ROS_Outputs" >}}) are also available:

* Video previews are published via a webserver on port 8080


The launch files takes a number of args:

| Arg                      |                                                                                                                                                                                                                                                                                                                                                 |
| ------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `do_stereo`              | If `true`, stereo disparity is calculated.  If `false`, it is not calculated.  Defaults to `true`                                                                                                                                                                                                                                               |
| `do_darknet`             | If `true`, a ROS node running the YOLO network is started.  If `false`, it is not run.  Defaults to `false`                                                                                                                                                                                                                                     |
| `do_cameras`             | If `true`, the Trisect camera driver is started.  If `false`, it is not started (suitable for re-processing pre-recorded data).  Defaults to `true`                                                                                                                                                                                             |
| `stereo_fps`             | The requested rate of the B&W stereo cameras.  Defaults to 10fps                                                                                                                                                                                                                                                                                |
| `stereo_gain`            | The requested gain of the B&W stereo cameras in the range of 1 to 2400.  Defaults to 800.                                                                                                                                                                                                                                                       |
| `stereo_exposure_max_ms` | The maximum allowable exposure time for the B&W cameras, in milliseconds.  By default  the cameras will run an autoexposure function;  this value sets the maximum allowable exposure from that autoexposure function.   At longer exposures, the images are more likely to blur with fast motion. Defaults to 10ms.                            |
| `environment`            | Defines which camera intrinsic calibrations are used.   The launch file uses the calibration files from [`trisect_sw`](https://gitlab.com/rsa-perception-sensor/trisect_sw) in the directory [`calibrations/<hostname>/<environment>/`](https://gitlab.com/rsa-perception-sensor/trisect_sw/-/tree/main/calibrations).   Defaults to `in_water` |

For example, to launch the software for testing `in_air` without stereo:

```
roslaunch trisect_sw trisect_nodelet.launch do_stereo:=false environment:=in_air
```

To stop the Trisect software, `ctrl-c` in the `roslaunch` window.

## Dynamic reconfiguration of Trisect params

Many of the Trisect parameters related to camera and stereo disparity function are accessible at runtime using ROS dynamic reconfigure.  In general we suggest accessing those parameters with standard ROS tools like [rqt's dynamic reconfigure plugin](https://wiki.ros.org/rqt_reconfigure)
