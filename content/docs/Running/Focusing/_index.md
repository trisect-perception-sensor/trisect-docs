---
title: "Focusing the Trisect"
weight: 10
resource:
 - src: "*.jpg"
---

Values on this page are calculated using [this Python notebook](https://gitlab.com/rsa-perception-sensor/trisect_analysis/-/blob/master/focusing_calculations/trisect_focusing_calculations.ipynb) in the [`trisect-analysis`](https://gitlab.com/rsa-perception-sensor/trisect_analysis/) repo.

The Trisect cameras are focused in air, with the front cover and domes removed.  Since the Trisect uses domed viewports, if the lenses are focused at a given distance *in air,* they will be in focus at a **greater** distance in water (or as it's normally said, objects in water are observed as a virtual image which is closer than their actual distance).

So, while the focusing procedure is completed in air, the goal is to achieve a particular focusing behavior once the sensor is sealed and in the water.

{{< imgproc src="trisect_on_focus_rail.jpg" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}

The focusing procedure is driven by the black-and-white cameras.  The color camera is focused to the same distance but we are less concerned with the details of its calibration.

Our procedure is (approximately):
- Find the virtual focal distance (`vfpp`) in air which corresponds to focusing at infinity in water.   Focal distances from infinity to zero (touching the dome) underwater will map from `vfpp` to zero (touching the dome) in air.
- Calculate the focal distance (`f_air`) in air which places `vfpp` at the far end of the depth of field.
- Calculate the near end of the depth of field (in air)
- Map that near DOF back into water (`near_water`).  Is it close enough?

If we follow this procedure and focus the lens at `f_air`, it will have a depth of field from `near_water` to infinity in water.   If `near_water` is not close enough, we can redo the calculations to move the depth of field closer, although this will move the far end of the DOF from infinity.

For the current dome geometry of Trisect, the virtual focal distance `vfpp` is **82.2mm** in front of the apex of the dome.

To put this at the far end of the depth of field, we focus the camera at 42.96mm from the apex of the lens.

The depth of field in air will be from **24.2 - 82.2 mm.** This maps to a depth of field of **51.6 mm - infinity** in water.

# Procedure

The values given above are relative to the domes.  We can transfer this the physical structure of the Trisect.  In this case we measure everything relative to the front face of the aluminum armature which holds the three cameras, the "camera plate." This provides a good surface to reference from.

There's one other detail.  Dome theory assumes the center of the dome is aligned with the entrance pupil of the lens.   We do all of our geometric calculations assuming this is true.   **However,** this alignment isn't generally true for the Trisect (particularly since the lenses move in-and-out as they're being focused).  Once the cameras are focused, shims can be installed behind the camera plate to align the lenses to the *real* domes.

So, based on the calculations above we want the far depth of field of the camera 82.2mm from the apex of the (ideal) dome, which is 170.2mm (6.7 in) from the face of the camera plate.  To achieve this we set up the focusing system (above) with the target this distance from plate.

We then adjust each of the three lenses until the image goes **just** out of focus.  This is obviously a qualitative judgement.   We can do a primitive cross-validation by measuring the distance from the camera plate to the lens bezel on each of the B&W camera.
