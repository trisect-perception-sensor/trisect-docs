---
title: "Userspace Software"
linkTitle: "User Software"
weight: 15
description: >
  Trisect Software.
---

Trisect runs on two chunks of custom software:

A custom kernel includes drivers and device tree configuration for the three cameras.  The specifics of setting up the core OS and kernel depend on which Trisect version you are using: ["Amigos"]({{< ref "Software_Amigos" >}}) or ["Bagsful"/"Crowd"]({{< ref "Software_Amigos" >}})

Trisect also includes a userspace software stack for controlling the cameras and interfacing with the ROS middleware.  It also includes some non-ROS tools.  Those are documented in this section.

The kernel exposes the cameras through standard Linux / NVidia interfaces (V4L, `libnvargus`), so standard Linux tools (`v4l2-ctl`, `gstreamer`) can be used with the cameras. However, for our [ROS-oriented workflow,]({{< relref "ROS_Software">}}) we use a set of [custom libraries.]({{<relref "#userspace">}})

# User-space software {id="userspace"}

Strictly speaking only the custom kernel is required to use the Trisect hardware platform.

However, we have written a number of packages which support our development.   All UW-written software is released under the [BSD license,](https://opensource.org/licenses/BSD-3-Clause) though we strongly encourage contributions back to the main code base.

* [libtrisect](https://gitlab.com/rsa-perception-sensor/libtrisect) is a C++ library which provides class interfaces to the Alvium cameras (using the V4L API) and the IMX477 using the GStreamer API (which in turn uses the `nvarguscamerasrc` plugin to route image data through the Argus ISP).  It also includes:
   * [triggerd](https://gitlab.com/rsa-perception-sensor/libtrisect/-/tree/master/triggerd) a userspace daemon for sending periodic GPIO pulses to trigger the Alvium cameras.  It is designed to run with elevated (soft real time) privileges.  "Regular" `libtrisect` controls it via a socket interface.

* [trisect_driver](https://gitlab.com/rsa-perception-sensor/trisect_driver) is a C++ ROS wrapper around `libtrisect`.

* [trisect_sw](https://gitlab.com/rsa-perception-sensor/trisect_sw) is the
* top-level meta-package.  It pulls in other third-party ROS nodes we use (e.g. GPU-accelerated stereo), and includes platform-levelk launchfiles.

One may wonder why we need a separate library when all three cameras are accessible through standard interfaces (for example, our V4L2 camera interface is very similar to [`usbcam`](http://wiki.ros.org/usb_cam), while our gstreamer API is based on [`gscam`](http://wiki.ros.org/gscam)).  In this case, the bespoke software offers better inter-camera control ... for example, the exposure and gain settings of one Alvium can be linked to track the settings of the other.  It also allows not just configuration of both monochrome cameras to be hardware triggered, but to actively monitor the synchronization between the two.
