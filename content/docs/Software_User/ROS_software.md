---
title: "ROS Software"
description: >
    ROS-specific software in detail.
---

The standard `trisect_sw` stack consists of a number of ROS nodes which provide the system's core functionality.   Actual use of the software is described in [Running trisect_sw]({{<relref "Running_Trisect_sw">}}) and the published ROS topics are described [here]({{<relref "ROS_Topics">}})

## trisect_sw

Repo: [https://gitlab.com/rsa-perception-sensor/trisect_sw](https://gitlab.com/rsa-perception-sensor/trisect_sw)

The package `trisect_sw` is the top-level meta-repo for the standard trisect_sw.  It contains files which include the other standard and custom software as dependencies, and contains the standard launchfiles.

It also contains calibration files for the APL-produced Trisect units.

## trisect_camera_driver

Repo: [https://gitlab.com/rsa-perception-sensor/trisect_camera_driver](https://gitlab.com/rsa-perception-sensor/trisect_camera_driver)

This ROS node interfaces with the three Trisect cameras, publishing the resulting imagery as ROS topics.   It also controls the cameras' exposure, gain, and frame rate settings.  It makes heavy use of the [libtrisect](https://gitlab.com/rsa-perception-sensor/libtrisect) which is a more generic non-ROS library providing hardware interfaces to the stereo Avlium cameras (via V4L), the RasPi camera (via GStreamer), and the hardware interfaces used to trigger the stereo cameras.

## Stereo pipeline

The stereo pipeline uses a custom node [gpu_stereo_image_proc](https://github.com/apl-ocean-engineering/gpu_stereo_image_proc) which performs the actual stereo block matching.
It also uses the [rectify](https://wiki.ros.org/image_proc#rectify) nodelet from ROS [image_proc](https://wiki.ros.org/image_proc#rectify).

## Disparity to point cloud

The output from [gpu_stereo_image_proc](https://github.com/apl-ocean-engineering/gpu_stereo_image_proc) is a ROS [DisparityImage](http://docs.ros.org/en/noetic/api/stereo_msgs/html/msg/DisparityImage.html).   [disparity_visualize](https://github.com/ActiveIntelligentSystemsLab/disparity_visualize) nodes produce ROS Image topics for visualizing the disparities, and [stereo_image_proc/points2](https://wiki.ros.org/stereo_image_proc) convert the disparity image to a point cloud.

**Note:** Since the disparity images are monochrome, the native point cloud is greyscale.   We are working on software to overlay the color image onto the point cloud.
## Darknet


## Non-ROS interfaces

In addition, the standard launchfile runs a few services which export ROS information to a non-ROS interface for simpler access.
### web_video_server

The standard launch files starts [trisect-specific fork of web_video_server](https://gitlab.com/rsa-perception-sensor/trisect_web_video_server).  This runs on port 8080 can provides a previews of all of the image topics (camera images, as well as disparity images) from the system.  Custom pages show left-right, left-color-right, and left-disparity-right streams together.
