---
title: "Hardware"
linkTitle: "Hardware"
weight: 20
description: >
  Detailed Hardware Description.
---

{{< imgproc src="Trisect_internals.jpg" cmd="Fit" opt="1024x768" >}}
{{< /imgproc >}}

The Trisect design is based on the NVidia Jetson NX module.  At present we are using the [development module,](https://developer.nvidia.com/embedded/jetson-xavier-nx-devkit) although a production version should use the [OEM module](https://developer.nvidia.com/embedded/jetson-xavier-nx).  It is mounted in the AntMicro [Nano Baseboard](https://github.com/antmicro/jetson-nano-baseboard) a compact Open Source design which include support for up to four CSI cameras, gigabit ethernet, an x4 NVMe slot, and USB connectivity:

![](https://github.com/antmicro/jetson-nano-baseboard/raw/master/Images/jetson-nano-layout.png)

CSI lanes are split between two connectors, J6 (on the long edge of the board) and J7 (on the short edge of the board).  Per [this table at Capable Robot](https://capablerobot.com/products/nx-baseboard/#tab-flex), with the Xavier NX, J6 can support one 4-lane camera and one 2-lane camera;  and J7 can support two 4-lanes cameras (currently only 2 lanes are used; see below).   We put the stereo pair on J7, and the color camera on the 4-lane connector on J7.

## Stereo cameras

The stereo pair is two Allied Vision [Alvium 1800-C-240m C-mount](https://www.alliedvision.com/en/products/alvium-configurator/alvium-1800-C/240/openhousing-c-mount-color#_configurator) cameras.  These cameras are based on the Sony [IMX392 sensor (PDF)](https://www.sony-semicon.co.jp/products/common/pdf/IMX392LLR_LQR_Flyer.pdf).   This is a 7.9mm diagonal global shutter sensor with a max resolution 1936 x 1216 (~2.4 MP).   It's relatively large 3.45um cells offer reasonable low-light capabilities.

The stereo pair on J7 is connected with a [dual camera, same-side flex](https://github.com/antmicro/alvium-flexible-csi-adapter).   This flex provides FETs for each of the VSYNC lines on the camera connectors -- these map to general-purpose input `EXT-GPIO2` (pin 5 on the 22-pin connector) on the Alvium C-1800 cameras.   For the Trisect design, we populate only the `Q1` FET corresponding to signal `C1_VSYNC` and tie the drain to both `C1_GPIO2` and `C2_GPIO2`  (see [schematics below]({{< relref "#schematics" >}}))

**Note:** AntMicro offers both same-side and opposite-side dual camera flexes -- with the baseboard and camera flexes on the same and opposite sides of the flex respectively.  They are functionallty equivalent and should both function identically.

## Color camera

The color camera is a stock [Raspberry Pi HQ camera](https://www.raspberrypi.org/products/raspberry-pi-high-quality-camera/) with a [Sony IMX477 sensor (PDF)](https://www.sony-semicon.co.jp/products/common/pdf/IMX477-AACK_Flyer.pdf).  This is a 7.8mm diagonal 12.3MP rolling shutter sensor, with an effective max resolution of 4056x3040.

The color camera is attached to connector J6 on the basedboard with an [Capable Robot 150mm 15-pin Pi flex](https://capablerobot.com/products/nx-baseboard/#tab-flex).  This connects the camera as "Camera 2" on the connector.   The Pi camera **does not** support hardware synchronization.  Instead the VSYNC connector on the flex is connected to the Pi camera's `enable` pin allowing reset of the camera.

The Ridgerun instructions for [using a Pi HQ camera with a Jetson](https://developer.ridgerun.com/wiki/index.php/Raspberry_Pi_HQ_camera_IMX477_Linux_driver_for_Jetson#Compatibility_with_NVIDIA®Jetson™_Platforms) mention that resistor R8 must be removed to properly reset the camera.  **This is not required for Trisect.**  The FET on the flex cable performs the necessary level shifting.

## GPIO Overview

Trisect uses just three GPIO lines, but they go through many translations.  The table below summarizes the GPIOs:

| Purpose                    | GPIO                    | GPIO Bank      | Linux name      | Baseboard signal | J6/J7 pin | Camera pin | Camera signal | Notes                                                                                                                                       |
| -------------------------- | ----------------------- | -------------- | --------------- | ---------------- | --------- | ---------- | ------------- | ------------------------------------------------------------------------------------------------------------------------------------------- |
| Left camera hardware sync  | SOC pin 114, GPIO0_PWDN | `GPIO3_PP.04`  | `gpiochip0.124` | `VSYNC_CAM2_1`   | J7.32     | C1.5       | `C1_GPIO2`    |                                                                                                                                             |
| Right camera hardware sync | SOC pin 130, GPIO06     | `GPIO3_PCC.03` | `gpiochip1.19`  | `VSYNC_CAM2_2`   | J7.34     | C2.5       | `C2_GPIO2`    | Typically don't populate FET on flex, instead tie the drain on Q1 to this drain together so `VSYNC_CAM1_2` triggers both cameras |
| Color camera reset         | SOC pin 216, GPIO11     | `GPIO3_PQ.06`  | `gpiochip0.134` | `VSYNC_CAM1_2`   | J6.34     | C1.11      | `ENABLE`      |                                                                                                                                             |



# Schematics

The following schematics are subject to the license of the original design files.   PDFs for the flex cables have been printed from the respective Kicad project files.

* [Antmicro Nanobaseboard schematics Rev 1.4.7 (PDF)](Jetson_Nano_Baseboard-Rev.1.4.7-schematic.pdf) from [AntMicro's Github repo](https://github.com/antmicro/jetson-nano-baseboard) released under the Apache-2 license.
* [Antmicro 100mm double-camera flex, same side (PDF)](alvium-flexible-csi-adapter-100mm-double-same-side.pdf) from [AntMicro's Github repo](https://github.com/antmicro/alvium-flexible-csi-adapter/tree/master/100mm-double-same-side) released under the Apache-2 license.
* [Capable Robot 150mm 15-pin Pi flex (PDF)](capablerobot_CRAJEA_Pi-Flex_15pin_150mm.pdf) from the [Capable Robot Github repo](https://github.com/CapableRobot/Flex-Cables/tree/main/Host-50pin/Pi-Flex_15pin_150mm) release under the MIT license.
* [Raspberry PI HQ schematics (PDF)](hq-camera-schematics.pdf) from the [Raspberry Pi Foundation](https://www.raspberrypi.com/documentation/accessories/camera.html#schematics)


# Known issues

* The Alvium cameras currently run with 2 CSI lanes.  We've been unable to get the cameras to run with 4 CSI lanes.
* The FET in the flex inverts the sense of the reset line for the Pi camera (0 is "on", 1 is "off").   This polarity is hardcoded in the Ridgerun and Nvidia IMX477 drivers;  however, the custom kernel in [Bagsful/Crowd]({{< ref "Software_Bagsful" >}}) addresses this issue.
