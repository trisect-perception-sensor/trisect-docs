---
title: "I2C Sensors"
linkTitle: "I2C Sensors"
weight: 20
description: "Supplemental I2C Sensors in Trisect"
---

Trisect includes a number of supplemental sensors on an I2C bus.  Unfortunately, the AntMicro baseboard doesn't expose any of the hardware I2C buses provided by the NX processor module.   Instead, we use an [Adafruit](https://www.adafruit.com/) dev board as a USB-to-I2C bridge.  While it seems silly, it lets us leverage their [Stemma QT](https://www.adafruit.com/category/1018) standard to use COTS dev boards for a variety of sensors.

The sensors are mounted on a removeable "experimenters plate" which provides a small volume for additional sensors to be installed.

Firmware for the RP2040 board and the supporting ROS interfaces are in the [trisect_i2c_sensors](https://gitlab.com/rsa-perception-sensor/trisect_i2c_sensors) repo at Gitlab.

# Hardware

The system uses a [Trinkey RP2040](https://www.adafruit.com/product/5056) as a USB-to-I2C gateway.   The board uses a small Circuitpython script to read the I2C sensors and transfer the data over the `/dev/ttyACMx` virtual serial port.

There are currently two sensors on the I2C chain.

 * A [BNO085](https://www.adafruit.com/product/4754) orientation sensor.
 * A [BME280](https://www.adafruit.com/product/2652) temperature/pressure/humidity sensor.

Two of the ADCs (2 and 3) on the QtPy are used a leak detectors.


# Interface

The current firmware prints six NMEA-like sentences on the virtual serial port `/dev/ttyACMx` provided by the Trinkey.

* `$TSQUT,x,y,z,w` is the current attitude of the BNO085 as a quaternion.
* `$TSACL,x,y,z` is the current accelerometer reading from the BN085
* `$TSGYR,x,y,z` is the current gyro readings from the BNO085
* `$TSMAG,x,y,z` is the current 3-axis magnetometer readings from the BNO085
* `$TSBME,t,h,p` is the current (t)emperature, relative (h)umidity, and (p)ressure from the BME280
* `$TSLEK,a,b` is the voltage on the two leak channels.   A voltage near zero is expected when dry, and near 3.3V when a leak is detected.

Each of the three sensors are read at different rates -- these rates are hard-coded in the [QtPy firmware]](https://gitlab.com/rsa-perception-sensor/trisect_i2c_sensors).  The BNO085 is read at 5Hz, while BME280 and leak sensors are read at 1Hz.


Here's a sample data output:

```
$TSQUT,-0.400452,-0.146423,-0.838867,0.338379
$TSACL,7.738281,1.187500,5.898438
$TSGYR,-0.007813,0.000000,-0.001953
$TSMAG,-24.750001,-43.312502,8.750000
$TSBME,17.477143,56.785569,1012.749314
$TSLEK,0.01,0.01
```

# ROS interface

A small ROS node reads the sensor and publishes the data.  See [ROS Software]({{< relref "ROS_software" >}})
