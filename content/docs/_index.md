---
title: "Documentation"
linkTitle: "Documentation"
weight: 20
menu:
  main:
    weight: 20
---

{{< imgproc src="Trisect_open.jpg" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}

> **Note:** With the upgrade to [Bagsful/Crowd]({{< ref "Versions" >}}), the documentation will be in flux for a bit.  Please [email me](mailto:amarburg@uw.edu) with questions or post an issue to [trisect_sw](https://gitlab.com/rsa-perception-sensor/trisect_sw/-/issues).

Trisect is a three-camera perception sensor based on the [NVidia Jetson NX processor,](https://developer.nvidia.com/embedded/jetson-xavier-nx) [AntMicro NX Baseboard,](https://capablerobot.com/products/nx-baseboard/) and a combination of [Allied Vision Alvium](https://www.alliedvision.com/en/products/embedded-vision-solutions/) and Raspberry-Pi compatible cameras.

Our research is focused on underwater perception, so a core element of Trisect is a shallow water waterproof housing designed and manufactured by [The Sexton Co](https://thesextonco.com/), however it is not an integral component to the functioning of the Jetson and cameras (other than acting as a heatsink for the Jetson processor) and much of the design could be re-purposed for other applications.

{{< imgproc src="Trisect_closed.jpg" cmd="Fit" opt="800x600" >}}
{{< /imgproc >}}

To the greatest extent possible, the design is made up of commercially available hardware, and Open Source software.   We hope the Trisect design is an accessible base or starting point for other novel Jetson-based camera projects.    We know many others are doing the same but during our initial planning we were frustrated by the absence of worked, fully documented open-source examples of multi-camera systems.

## Design Fundamentals

The core of Trisect is a pair of global shutter, hardware synchronized [Allied Vision Alvium](https://www.alliedvision.com/en/products/embedded-vision-solutions/) machine vision cameras.   For Trisect we use monochrome cameras based on the [3MP Sony IMX392](https://www.alliedvision.com/en/products/alvium-configurator/alvium-1800-C/319/openhousing-c-mount-color), however it could be adapted to other Alvium CSI cameras.  These cameras connect to the Jetson Xavier NX via the Open Source [AntMicro NX Baseboard,](https://capablerobot.com/products/nx-baseboard/) providing a platform for capturing and recording data, as well as performing realtime stereo depth map generation, machine learning, etc.

We also use a color camera based on the Sony IMX477 -- [a Raspberry Pi HQ camera.](https://www.raspberrypi.org/products/raspberry-pi-high-quality-camera/) --  as a companion to the monochrome stereo pair.   This is a 12MP, rolling shutter color camera which can provide detailed, color context.

### Advantages to Three-Camera Design

* Each camera can be tuned to its job (e.g. monochrome camera exposure/gain can be tuned for stereo generation rather than human legibility).
* All three sensors are roughly the same physical size (IMX392 at 8.9mm diagonal versus IMX477 at 7.9mm diagonal) giving the all cameras a similar FOV for the same lenses.
* The Raspi camera is a low cost, high resolution sensor.

### Disadvantages to Three-Camera Design

* RasPi camera is roling shutter and cannot be externally triggered ... so the color image cannot be hardware synchronized to the stereo pair.
* Three cameras create additional size and complexity -- though the Raspi camera is optional.
